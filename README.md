This project is used to build Cameo using podman / docker.

It could be used to build any OpenRA mod which uses make and package-all.sh.

## Checking out the code

```
git clone https://gitea.burntworld.ca/kienan/cameo-build.git
```

To get the Cameo source code, clone one of the following repos;

* [Cameo (releases)](https://github.com/Zeruel87/Cameo-mod.git)
* [Cameo (dev)](https://github.com/Elpollo315/Cameo-mod.git)
* [Cameo Reloaded](https://github.com/Carloo999/CameoReloaded.git)

## Building container image

```
make container
```

## Building Cameo

To produce builds with complete packages:

```
make build
```

To build the executables only;

```
make build-dev
```
