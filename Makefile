CWD=$(shell pwd)
CAMEO_DIR?=$(CWD)/Cameo-mod
VERSION?=$(shell git --git-dir=$(CAMEO_DIR)/.git --work-tree=$(CAMEO_DIR) describe --tags)
VERSION?=$(shell git --git-dir=$(CAMEO_DIR)/.git --work-tree=$(CAMEO_DIR) rev-parse HEAD)

.phony: build-dev build all container build-linux

all: container build

container:
	podman build -t cameo-build:latest .

build:
	podman run --rm -v $(CAMEO_DIR):/workdir -e APPIMAGE_EXTRACT_AND_RUN=1 -e RELEASE_VERSION=$(VERSION) localhost/cameo-build:latest

build-dev:
	podman run --rm -v $(CAMEO_DIR):/workdir localhost/cameo-build:latest

build-linux:
	podman run --rm -v $(CAMEO_DIR):/workdir -e APPIMAGE_EXTRACT_AND_RUN=1 -e RELEASE_VERSION=$(VERSION) localhost/cameo-build:latest make
	podman run --rm -v $(CAMEO_DIR):/workdir -e APPIMAGE_EXTRACT_AND_RUN=1 -e RELEASE_VERSION=$(VERSION) localhost/cameo-build:latest /bin/bash -c "cd build && ../packaging/linux/buildpackage.sh $(VERSION)"
